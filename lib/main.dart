import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_firestore_crud2/pages/home_page.dart';

void main() {
  //ADD FOLLOWING LINE FIRESTORE
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //ADD FOLLOWING LINE FIRESTORE
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initialization, builder: (context, snapshot) {
          if(snapshot.hasError){
            print('something went wrong');
          }
          if(snapshot.connectionState == ConnectionState.done){
            return MaterialApp(
              title: 'Firestore CRUD',
              theme: ThemeData(
                primarySwatch: Colors.red,
              ),
              debugShowCheckedModeBanner: false,
              home: HomePage()
              ,
            );
          }
          return CircularProgressIndicator();
    });


  }
}
